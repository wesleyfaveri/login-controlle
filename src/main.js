import React from 'react';
import { BackButton, ThemeProvider } from './components';
import { setJSExceptionHandler } from 'react-native-exception-handler';
import { View, StyleSheet, StatusBar, Alert } from 'react-native';
import { NativeRouter, Route } from 'react-router-native';
import FlashMessage from 'react-native-flash-message';
import { Root } from 'native-base';
import { Provider } from 'react-redux';
import store from './store';
import { InitialPage, LoginPage, CreateAccountPage, HomePage } from './containers';

setJSExceptionHandler((err, isFatal) => {
  if (err) {
    Alert.alert(
      'Atenção',
      `${err.message}`,
      [{ text: 'Fechar' }]
    );
  }
});

const Main = () => (
  <ThemeProvider>
    <Provider store={store}>
      <Root>
        <NativeRouter>
          <StatusBar hidden />
          <View style={styles.view}>
            <Route exact path="/" component={InitialPage} />
            <Route exact path="/login" component={LoginPage} />
            <Route exact path="/create-account" component={CreateAccountPage} />
            <Route exact path="/home" component={HomePage} />

            <FlashMessage position="bottom" />
          </View>
          <BackButton />
        </NativeRouter>
      </Root>
    </Provider>
  </ThemeProvider>
);

const styles = StyleSheet.create({
  view: {
    flex: 1,
  },
});

export default Main;
