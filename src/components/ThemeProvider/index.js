import React from 'react';
import { StyleProvider } from 'native-base';
import getTheme from '../../theme/components';
import material from '../../theme/variables/material';

const ThemeProvider = ({ children }) => (
  <StyleProvider style={getTheme(material)}>
    {children}
  </StyleProvider>
);

export default ThemeProvider;
