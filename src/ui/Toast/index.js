import { Platform } from 'react-native';
import { showMessage } from 'react-native-flash-message';

const show = (props) => {
  if (typeof(props) === 'string') {
    props = {
      text: props,
    };
  }

  return showMessage({
    message: props.text,
    icon: 'auto',
    hideStatusBar: (Platform.OS === 'ios'),
    ...props,
  });
};

export default {
  show,
};
