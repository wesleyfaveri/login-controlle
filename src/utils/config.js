export const PATH_TELA_PRINCIPAL = '/';
export const URL_TERMOS_DE_USO = 'https://www.controlle.com/termos-de-uso';
export const URL_RECUPERAR_SENHA = 'https://www.controlle.com/password/new';
export const URL_API = 'https://app.controlle.com';
