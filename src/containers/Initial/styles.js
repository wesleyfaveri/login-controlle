import { CORES } from '@components';

export default {
  view: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  titulo: {
    fontSize: 20,
    fontWeight: 'bold',
    color: CORES.text,
    marginBottom: 45,
  },
  buttonCadastro: {
    backgroundColor: CORES.primary,
    width: '75%',
    alignItems: 'center',
    borderRadius: 3,
  },
  textButtonCadastro: {
    color: '#fff',
    fontWeight: 'bold',
  },
  buttonLogin: {
    marginTop: 45,
  },
  textButtonLogin: {
    color: CORES.placeHolderColor,
  }
};
