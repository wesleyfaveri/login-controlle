import { URL_API } from '@utils/config';

export const post = (api, body, errorJson = false) => {
  const path = URL_API + api;
  const headers = getHeaders();

  return fetch(path, {
    method: 'POST',
    headers,
    body: JSON.stringify(body),
  }).then(async (response) => checkRequisicao(response, body, headers, errorJson));
};

export const get = (api, errorJson = false) => {
  const path = URL_API + api;
  const headers = getHeaders();

  return fetch(path, {
    method: 'GET',
    headers,
  }).then((response) => checkRequisicao(response, {}, headers, errorJson));
};

const checkRequisicao = async (response, body = {}, headers = {}, errorJson = false) => {
  let responseObject = await response.text();

  if (errorJson) {
    try {
      responseObject = JSON.parse(responseObject);
    } catch (err) {
      //lint
    }
  }

  if (response.ok) return responseObject;

  throw (errorJson) ? responseObject : new Error('Erro na requisição.');
};

const getHeaders = () => ({
  'Accept': 'application/json',
  'Content-Type' : 'application/json',
});
