export default {
  loadingView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    position: 'absolute',
    left: 0,
    top: 0,
    right: 0,
    bottom: 0,
  },
  textCarregando: {
    fontWeight: '600',
    textAlign: 'center',
    color: 'white',
    marginBottom: 5,
    letterSpacing: 2,
  },
  spinner: {
    height: 50,
  }
};
