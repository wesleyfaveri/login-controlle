import { AppRegistry } from 'react-native';
import Main from './src/main';
import { name as appName } from './app';

AppRegistry.registerComponent(appName, () => Main);
