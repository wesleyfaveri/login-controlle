import { CORES } from '@components';

export default {
  content: {
    flexGrow : 1,
    justifyContent : 'center',
  },
  header: {
    height: null,
    paddingTop: 0,
  },
  icon: {
    color: CORES.placeHolderColor,
  },
  titulo: {
    alignSelf: 'center',
    fontSize: 20,
    fontWeight: 'bold',
    color: CORES.text,
    marginBottom: 50,
  },
  item: {
    marginBottom: 25,
    marginLeft: 30,
    marginRight: 30,
  },
  input: {
    height: null,
    paddingVertical: 2,
  },
  iconInput: {
    alignSelf: 'center',
    width: 32,
  },
  iconError: {
    color: 'red',
  },
  buttonEntrar: {
    backgroundColor: CORES.primary,
    margin: 25,
  },
  textEntrar: {
    color: '#fff',
    fontWeight: 'bold',
  },
  buttonEsqueciSenha: {
    alignSelf: 'center'
  },
  textEsqueciSenha: {
    color: CORES.placeHolderColor,
  },
};
