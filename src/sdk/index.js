import { post } from './methods';

export const login = (user) => post('/api/site/authenticate', { user }, true);
export const createAccount = (body) => post('/api/site/registrations/business', body, true);
