import React from 'react';
import { View, Text } from 'react-native';
import { Touchable } from '@components';
import styles from './styles';

const Initial = ({ history }) => (
  <View style={styles.view}>
    <Text style={styles.titulo}>Crie sua conta grátis</Text>

    <Touchable style={styles.buttonCadastro} onPress={() => history.push('/create-account')}>
      <Text style={styles.textButtonCadastro}>Cadastre-se</Text>
    </Touchable>

    <Touchable useDefaultStyle={false} style={styles.buttonLogin} onPress={() => history.push('/login')} >
      <Text style={styles.textButtonLogin}>Já tenho conta</Text>
    </Touchable>
  </View>
);



export default Initial;
