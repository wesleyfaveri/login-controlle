import { isEmailValid } from '@utils';

export const gotError = (error, field) => error.find((item) => item === field);
export const getExtraStyle = (defaultColor, focus, current) => (focus === current) ? { borderBottomWidth: 2, borderColor: defaultColor } : {};

const DEFAULT_INPUT_ERROR_MESSAGE = 'Não pode ficar em branco';

export const checkInput = ({ nome, email, senha }) => {
  if ((nome) && (nome.length > 0)) {
    if ((email) && (email.length > 0)) {
      if (isEmailValid(email)) {
        if ((senha) && (senha.length > 0)) {
          if (senha.length < 6) {
            return handleError('senha', 'Muito curto, o mínimo é 6 caracteres');
          } else {
            return null;
          }
        } else {
          return handleError('senha', DEFAULT_INPUT_ERROR_MESSAGE);
        }
      } else {
        return handleError('email', 'E-mail inválido');
      }
    } else {
      return handleError('email', DEFAULT_INPUT_ERROR_MESSAGE);
    }
  } else {
    return handleError('nome', DEFAULT_INPUT_ERROR_MESSAGE);
  }
};

const handleError = (field, message) => ({ field: field, message });

export const getInputName = (field) => {
  switch (field) {
    case 'nome': return 'inputNome';
    case 'email': return 'inputEmail';
    case 'senha': return 'inputSenha';
    default: return 'inputNome';
  }
};

export const getBody = (dados) => {
  const { email, senha } = dados;

  return {
    registration: { email, password: senha, 'password_confirmation': senha },
    'terms_of_use': '1'
  };
};

const existsKey = (array, key) => array.some((item) => item === key);

export const getBetterErrorMessage = (message) => {
  const keys = Object.keys(message);

  if (existsKey(keys, 'email')) {
    const errorEmail = message.email;

    if (errorEmail) {
      if (existsKey(errorEmail, 'já está em uso')) {
        return 'Email já cadastrado, esqueceu a senha?';
      } else {
        return 'Há algo de errado com o email selecionado';
      }
    }
  }

  if (existsKey(keys, 'password')) {
    return 'Há algo de errado com a senha selecionada';
  }

  if (existsKey(keys, 'error')) {
    return 'E-mail inválido';
  }
};
