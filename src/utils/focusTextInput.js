/*global console*/
import { findNodeHandle } from 'react-native';
import TextInputState from 'react-native/lib/TextInputState';
import PropTypes from 'prop-types';

const focusInput = (node) => {
  try {
    TextInputState.focusTextInput(findNodeHandle(node));
  } catch (err) {
    console.warn("Couldn't focus text input: ", err.message);
  }
};

focusInput.propTypes = {
  node: PropTypes.node.isRequired,
};

export default focusInput;
