import { combineReducers } from 'redux';
import createAccountReducer from './containers/CreateAccount/reducer';
import loginReducer from './containers/Login/reducer';

const reducers = combineReducers({
  createAccount: createAccountReducer,
  login: loginReducer,
});

export default reducers;
