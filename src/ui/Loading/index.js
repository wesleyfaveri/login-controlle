import React from 'react';
import PropTypes from 'prop-types';
import { View, ActivityIndicator } from 'react-native';
import styles from './styles';

const Loading = ({ visible, style, color }) => {
  const spinnerColor = (color) ? color : 'white';

  return (visible) ? (
    <View style={{ ...styles.loadingView, ...style }}>
      <ActivityIndicator color={spinnerColor} style={styles.spinner}/>
    </View>
  ) : null;
};

Loading.propTypes = {
  visible: PropTypes.bool,
  style: PropTypes.object,
  color: PropTypes.string,
};

Loading.defaultProps = {
  visible: false,
};

export default Loading;
