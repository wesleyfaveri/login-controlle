import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Text, Linking } from 'react-native';
import { CORES, Touchable } from '@components';
import { Container, Header, Left, Right, Content, Body, Button, Icon, Form, Item, Input } from 'native-base';
import { gotError, checkInput, getExtraStyle } from './rules';
import { focusTextInput } from '@utils';
import { login } from './actions';
import { Loading, Toast, BackButton } from '@ui';
import { URL_RECUPERAR_SENHA } from '@utils/config';
import styles from './styles';

class Login extends Component {
  state = {
    email: '',
    senha: '',
    error: [],
    focus: 1,
  };

  handleOnPressEntrar = () => {
    const { email, senha } = this.state;

    const error = checkInput(email, senha);

    if (error) {
      Toast.show({ text: error.message });
      return this.setState({ error: [error.field] });
    } else {
      this.setState({ error: [] });

      return this.handleLogin();
    }
  };

  handleLogin = () => {
    const { email, senha } = this.state;
    const { onLogin } = this.props;

    onLogin(email, senha);
  };

  componentDidUpdate(prevProps) {
    const { history, error, sucesso } = this.props;

    if ((error) && (error !== prevProps.error)) {
      Toast.show({ text: error });
    }

    if ((sucesso) && (sucesso !== prevProps.sucesso)) {
      return history.replace('/home', { sucesso });
    }
  }

  handleOpenRecuperarSenha = () => {
    Linking.canOpenURL(URL_RECUPERAR_SENHA).then((canOpen) => {
      if (canOpen) {
        return Linking.openURL(URL_RECUPERAR_SENHA);
      } else {
        Toast.show({ text: 'Não foi possível abrir o link' });
      }
    });
  }

  render() {
    const { email, senha, error, focus } = this.state;
    const { loading } = this.props;

    return (
      <Container style={styles.container}>
        <Header transparent style={styles.header}>
          <Left>
            <BackButton />
          </Left>
          <Body />
          <Right />
        </Header>
        <Content contentContainerStyle={styles.content}>
          <Text style={styles.titulo}>Acesse sua conta</Text>

          <Form>
            <Item style={[styles.item, getExtraStyle(CORES.activeFieldColor, focus, 1)]}>
              <Icon style={[styles.icon, styles.iconInput]} active name='mail' />
              <Input
                style={styles.input}
                autoFocus
                placeholderTextColor={CORES.placeHolderColor}
                placeholder='Email'
                autoCorrect={false}
                autoCapitalize='none'
                returnKeyType='next'
                keyboardType='email-address'
                onChangeText={(input) => this.setState({ email: input })}
                value={email}
                onSubmitEditing={() => focusTextInput(this.inputSenha)}
                onFocus={()=> this.setState({ focus: 1 })}
              />
              {gotError(error, 'email') && <Icon name='alert' style={styles.iconError} />}
            </Item>

            <Item style={[styles.item, getExtraStyle(CORES.activeFieldColor, focus, 2)]}>
              <Icon style={[styles.icon, styles.iconInput]} active name='lock' />
              <Input
                ref={(r) => { this.inputSenha = r; }}
                style={styles.input}
                placeholderTextColor={CORES.placeHolderColor}
                placeholder='Senha'
                autoCorrect={false}
                autoCapitalize='none'
                secureTextEntry
                onChangeText={(input) => this.setState({ senha: input })}
                value={senha}
                onFocus={()=> this.setState({ focus: 2 })}
              />
              {gotError(error, 'senha') && <Icon name='alert' style={styles.iconError} />}
            </Item>

            <Button full style={styles.buttonEntrar} onPress={this.handleOnPressEntrar}>
              <Text style={styles.textEntrar}>ENTRAR</Text>
            </Button>
            <Touchable useDefaultStyle={false} style={styles.buttonEsqueciSenha} onPress={this.handleOpenRecuperarSenha}>
              <Text style={styles.textEsqueciSenha}>Esqueci a senha</Text>
            </Touchable>
          </Form>
        </Content>
        <Loading visible={loading} />
      </Container>
    );
  }
}

const mapStateToProps = ({ login }) => ({
  sucesso: login.get('sucesso'),
  loading: login.get('loading'),
  error: login.get('error'),
});

const mapDispatchToProps = (dispatch) => ({
  onLogin: (email, senha) => dispatch(login(email, senha)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Login);
