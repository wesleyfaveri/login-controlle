import { Map } from 'immutable';

const initialState = Map({
  loading: false,
  sucesso: false,
  error: false,
});

const handleLogin = (state, action) => {
  return state
    .set('loading', true)
    .set('sucesso', false)
    .set('error', false);
};

const handleLoginSuccess = (state, action) => {
  return state
    .set('loading', false)
    .set('sucesso', action.retorno)
    .set('error', false);
};

const handleLoginError = (state, action) => {
  return state
    .set('loading', false)
    .set('sucesso', false)
    .set('error', action.error);
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case 'LOGIN':
      return handleLogin(state, action);
    case 'LOGIN_SUCCESS':
      return handleLoginSuccess(state, action);
    case 'LOGIN_ERROR':
      return handleLoginError(state, action);

    default:
      return state;
  }
};

export default reducer;
