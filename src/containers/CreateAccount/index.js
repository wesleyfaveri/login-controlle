import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Text, Linking } from 'react-native';
import { CORES, Touchable } from '@components';
import { Container, Header, Left, Right, Content, Body, Button, Icon, Form, Item, Input } from 'native-base';
import { gotError, checkInput, getExtraStyle, getInputName } from './rules';
import { focusTextInput } from '@utils';
import { createAccount } from './actions';
import { Loading, Toast, BackButton } from '@ui';
import { URL_TERMOS_DE_USO } from '@utils/config';
import styles from './styles';

class CreateAccount extends Component {
  state = {
    empresa: '',
    nome: '',
    email: '',
    senha: '',
    error: [],
    focus: 1,
  };

  handleOnPressCreate = () => {
    const dados = this.state;

    const error = checkInput(dados);

    if (error) {
      Toast.show({ text: error.message });
      this.setState({ error: [error.field] });
      return focusTextInput(this[getInputName(error.field)]);
    } else {
      this.setState({ error: [] });

      return this.handleCreate();
    }
  };

  handleCreate = () => this.props.onCreateAccount(this.state);

  componentDidUpdate(prevProps) {
    const { history, error, sucesso } = this.props;

    if ((error) && (error !== prevProps.error)) {
      Toast.show({ text: error });
    }

    if ((sucesso) && (sucesso !== prevProps.sucesso)) {
      return history.replace('/home', { sucesso });
    }
  }

  handleOpenTermoDeUso = () => {
    Linking.canOpenURL(URL_TERMOS_DE_USO).then((canOpen) => {
      if (canOpen) {
        return Linking.openURL(URL_TERMOS_DE_USO);
      } else {
        Toast.show({ text: 'Não foi possível abrir o link' });
      }
    });
  }

  render() {
    const { empresa, nome, email, senha, error, focus } = this.state;
    const { loading } = this.props;

    return (
      <Container style={styles.container}>
        <Header transparent style={styles.header}>
          <Left>
            <BackButton />
          </Left>
          <Body />
          <Right />
        </Header>
        <Content contentContainerStyle={styles.content}>
          <Text style={styles.titulo}>Crie usa conta grátis</Text>

          <Form>
            <Item style={[styles.item, getExtraStyle(CORES.activeFieldColor, focus, 1)]}>
              <Icon style={[styles.icon, styles.iconInput]} active name='briefcase' />
              <Input
                style={styles.input}
                autoFocus
                placeholderTextColor={CORES.placeHolderColor}
                placeholder='Empresa'
                returnKeyType='next'
                onChangeText={(input) => this.setState({ empresa: input })}
                value={empresa}
                onSubmitEditing={() => focusTextInput(this.inputNome)}
                onFocus={()=> this.setState({ focus: 1 })}
              />
              {gotError(error, 'empresa') && <Icon name='alert' style={styles.iconError} />}
            </Item>

            <Item style={[styles.item, getExtraStyle(CORES.activeFieldColor, focus, 2)]}>
              <Icon style={[styles.icon, styles.iconInput]} active name='person' />
              <Input
                ref={(r) => { this.inputNome = r; }}
                style={styles.input}
                placeholderTextColor={CORES.placeHolderColor}
                placeholder='Nome'
                returnKeyType='next'
                onChangeText={(input) => this.setState({ nome: input })}
                value={nome}
                onSubmitEditing={() => focusTextInput(this.inputEmail)}
                onFocus={()=> this.setState({ focus: 2 })}
              />
              {gotError(error, 'nome') && <Icon name='alert' style={styles.iconError} />}
            </Item>

            <Item style={[styles.item, getExtraStyle(CORES.activeFieldColor, focus, 3)]}>
              <Icon style={[styles.icon, styles.iconInput]} active name='mail' />
              <Input
                ref={(r) => { this.inputEmail = r; }}
                style={styles.input}
                placeholderTextColor={CORES.placeHolderColor}
                placeholder='Email'
                autoCorrect={false}
                autoCapitalize='none'
                returnKeyType='next'
                keyboardType='email-address'
                onChangeText={(input) => this.setState({ email: input })}
                value={email}
                onSubmitEditing={() => focusTextInput(this.inputSenha)}
                onFocus={()=> this.setState({ focus: 3 })}
              />
              {gotError(error, 'email') && <Icon name='alert' style={styles.iconError} />}
            </Item>

            <Item style={[styles.item, getExtraStyle(CORES.activeFieldColor, focus, 4)]}>
              <Icon style={[styles.icon, styles.iconInput]} active name='lock' />
              <Input
                ref={(r) => { this.inputSenha = r; }}
                style={styles.input}
                placeholderTextColor={CORES.placeHolderColor}
                placeholder='Senha'
                autoCorrect={false}
                autoCapitalize='none'
                secureTextEntry
                onChangeText={(input) => this.setState({ senha: input })}
                value={senha}
                onFocus={()=> this.setState({ focus: 4 })}
              />
              {gotError(error, 'senha') && <Icon name='alert' style={styles.iconError} />}
            </Item>

            <Button full style={styles.buttonCreate} onPress={this.handleOnPressCreate}>
              <Text style={styles.textCreate}>Começar</Text>
            </Button>
            <Touchable useDefaultStyle={false} style={styles.buttonTermoUso} onPress={this.handleOpenTermoDeUso}>
              <Text style={styles.textTermoUso}>Termo de uso</Text>
            </Touchable>
          </Form>
        </Content>
        <Loading visible={loading} />
      </Container>
    );
  }
}

const mapStateToProps = ({ createAccount }) => ({
  sucesso: createAccount.get('sucesso'),
  loading: createAccount.get('loading'),
  error: createAccount.get('error'),
});

const mapDispatchToProps = (dispatch) => ({
  onCreateAccount: (dados) => dispatch(createAccount(dados)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CreateAccount);
