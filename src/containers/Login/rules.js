import { isEmailValid } from '@utils';

export const gotError = (error, field) => error.find((item) => item === field);
export const getExtraStyle = (defaultColor, focus, current) => (focus === current) ? { borderBottomWidth: 2, borderColor: defaultColor } : {};

const DEFAULT_INPUT_ERROR_MESSAGE = 'Não pode ficar em branco';

export const checkInput = (email, senha) => {
  if ((email) && (email.length > 0)) {
    if (isEmailValid(email)) {
      if ((senha) && (senha.length > 0)) {
        return null;
      } else {
        return handleError('senha', DEFAULT_INPUT_ERROR_MESSAGE);
      }
    } else {
      return handleError('email', 'E-mail inválido');
    }
  } else {
    return handleError('email', DEFAULT_INPUT_ERROR_MESSAGE);
  }
};

const handleError = (field, message) => ({ field: field, message });

export const getBetterErrorMessage = (message) => {
  switch (message) {
    case 'invalid_user': return 'Usuário ou senha incorreto.';
    default: return 'Usuário inválido';
  }
};
