import { useEffect } from 'react';
import { BackHandler } from 'react-native';

export const useBackHandler = (handler) => {
  const handleBackPressed = () => {
    return handler();
  };

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', handleBackPressed);
    return () => BackHandler.removeEventListener('hardwareBackPress', handleBackPressed);
  }, []);

  return;
};
