import React from 'react';
import { TouchableHighlight } from 'react-native';
import PropTypes from 'prop-types';
import { CORES } from '@components';
import color from 'color';

const defaultStyle = {
  padding: 15,
  backgroundColor: CORES.primary,
};

const fullStyle = {
  width: '100%',
  alignItems: 'center',
};

const useStyle = (use, style) => (use) ? style : {};
const getStyle = (useDefaultStyle, full, style) => [useStyle(full, fullStyle), useStyle(useDefaultStyle, defaultStyle)].concat(style);
const getUnderlayColor = (buttonColor) => (buttonColor) ? color(buttonColor).lighten(.2) : 'transparent';

const Touchable = ({ children, useDefaultStyle, full, style, ...otherProps }) => {
  const buttonStyle = getStyle(useDefaultStyle, full, style);
  const underlayColor = getUnderlayColor(style.backgroundColor);

  return (
    <TouchableHighlight style={buttonStyle} underlayColor={underlayColor} {...otherProps}>
      {children}
    </TouchableHighlight>
  );
};

Touchable.propTypes = {
  useDefaultStyle: PropTypes.bool,
  full: PropTypes.bool,
  style: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.array,
  ]),
};

Touchable.defaultProps = {
  useDefaultStyle: true,
  full: false,
  style: {},
};

export default Touchable;
