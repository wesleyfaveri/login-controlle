import React from 'react';
import { View, Text } from 'react-native';
import styles from './styles';

const Home = ({ location }) => (
  <View style={styles.view}>
    <Text style={styles.text}>{location.state.sucesso.follow}</Text>
  </View>
);


export default Home;
