import { BackHandler } from 'react-native';
import { useBackHandler } from '@utils/hooks';
import { withRouter } from 'react-router-native';
import { PATH_TELA_PRINCIPAL } from '@utils/config';

const BackButton = ({ history }) => {
  const handleOnGoBack = () => {
    if (history.location.pathname === PATH_TELA_PRINCIPAL) {
      BackHandler.exitApp();
    } else {
      history.goBack();
    }

    return true;
  };
  useBackHandler(handleOnGoBack);

  return null;
};

export default withRouter(BackButton);
