import { takeEvery, put, call } from 'redux-saga/effects';
import { getBody, getBetterErrorMessage } from './rules';
import { createAccount } from '@sdk';

function* fetchCreateAccount(action) {
  try {
    const retorno = yield call(createAccount, getBody(action.dados));

    yield put({ type: 'CREATE_ACCOUNT_SUCCESS', retorno });
  } catch (err) {
    yield put({ type: 'CREATE_ACCOUNT_ERROR', error: getBetterErrorMessage(err) });
  }
}

export default [
  takeEvery('CREATE_ACCOUNT', fetchCreateAccount),
];
