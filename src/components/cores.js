export default {
  primary: '#0097FE',
  activeFieldColor: '#038D7E',
  text: '#636363',
  placeHolderColor: '#a9a9a9',
};
