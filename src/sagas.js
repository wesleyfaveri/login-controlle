import { all } from 'redux-saga/effects';

import loginSaga from './containers/Login/sagas';
import createAccountSaga from './containers/CreateAccount/sagas';

function* rootSaga() {
  yield all([
    ...loginSaga,
    ...createAccountSaga,
  ]);
}

export default rootSaga;
