import { Map } from 'immutable';

const initialState = Map({
  loading: false,
  sucesso: false,
  error: false,
});

const handleCreateAccount = (state, action) => {
  return state
    .set('loading', true)
    .set('sucesso', false)
    .set('error', false);
};

const handleCreateAccountSuccess = (state, action) => {
  return state
    .set('loading', false)
    .set('sucesso', action.retorno)
    .set('error', false);
};

const handleCreateAccountError = (state, action) => {
  return state
    .set('loading', false)
    .set('sucesso', false)
    .set('error', action.error);
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case 'CREATE_ACCOUNT':
      return handleCreateAccount(state, action);
    case 'CREATE_ACCOUNT_SUCCESS':
      return handleCreateAccountSuccess(state, action);
    case 'CREATE_ACCOUNT_ERROR':
      return handleCreateAccountError(state, action);

    default:
      return state;
  }
};

export default reducer;
