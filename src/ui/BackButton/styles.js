import { CORES } from '@components';

export default {
  icon: {
    color: CORES.placeHolderColor,
  },
};
