import { takeEvery, put, call } from 'redux-saga/effects';
import { login } from '@sdk';
import { getBetterErrorMessage } from './rules';

function* fetchLogin(action) {
  try {
    const { email, senha } = action;

    const retorno = yield call(login, { email, password: senha });

    yield put({ type: 'LOGIN_SUCCESS', retorno });
  } catch (err) {
    yield put({ type: 'LOGIN_ERROR', error: getBetterErrorMessage(err.error) });
  }
}

export default [
  takeEvery('LOGIN', fetchLogin),
];
