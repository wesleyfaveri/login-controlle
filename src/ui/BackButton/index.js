import React from 'react';
import PropTypes from 'prop-types';
import { BackHandler } from 'react-native';
import { withRouter } from 'react-router-native';
import { Button, Icon } from 'native-base';
import styles from './styles';
import { PATH_TELA_PRINCIPAL } from '@utils/config';

const BackButton = ({ onPress, history }) => {
  const handleOnGoBack = () => {
    if (onPress) {
      onPress();
    } else {
      if (history.location.pathname === PATH_TELA_PRINCIPAL) {
        BackHandler.exitApp();
      } else {
        history.goBack();
      }
    }
    return true;
  };

  return (
    <Button transparent onPress={handleOnGoBack}>
      <Icon name="arrow-back" style={styles.icon}/>
    </Button>
  );
};

BackButton.propTypes = {
  history: PropTypes.object.isRequired,
  onPress: PropTypes.func,
};

export default withRouter(BackButton);
